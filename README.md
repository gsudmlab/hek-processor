# HEK-Processor

* [PREREQUISITES](#markdown-header-prerequisites)
* [CLONE REPOSITORY](#markdown-header-clone-repository)
* [CONFIGURATION](#markdown-header-configuration)
* [Acknowledgment](#markdown-header-acknowledgment)
* [License](./LICENSE.txt)

# PREREQUISITES 
This project is a Java based project which utilizes Maven for dependency management.  To that end, both Java and Maven need to be installed on your machine prior to proceeding with any utilizing this project. The following are some helpful instructions for the prerequisites for this project.  

If the prerequisites are already installed, you can skip forward to the next section on how to download, compile, and use the installed library in your own project. Otherwise go to the [PREREQ](./PREREQ.md) instructions.

# CLONE REPOSITORY
Now that we have the prerequisites installed, we need to clone the repository to your local machine.

### Step 1: Go Home ###
Navigate to your home (~) directory.

	$ cd ~

### Step 2: Make a place to store repos ###
	
	$ mkdir repos

Or whatever you want to name it. This is just a folder to hold your git repositories, it won't be the final folder of the repository.

	$ cd ~/repos

Go to the place we will store the repository.

### Step 3: Clone the repo ###

	git clone https://username:password@bitbucket.org/gsudmlab/hek-processor.git

You will need to replace your username and password for the placeholders above.
	
# CONFIGURATION

The main config file is located in the ./config directory, and is named hek-processor.cfg.xml

A full listing of the lines lines in the config file and what they control are listed in the [CONFIG.md](CONFIG.md) file.

### There is an additional file located in ./config/tables.  

	The **attributes.tsv** file lists all of the solar event types and the attributes that each one contains.  
	



# Acknowledgment

 This work was supported in part by two NASA Grant Awards (No. NNX11AM13A, and No. NNX15AF39G),
 and one NSF Grant Award (No. AC1443061). The NSF Grant Award has been supported by funding 
 from the Division of Advanced Cyberinfrastructure within the Directorate for Computer and 
 Information Science and Engineering, the Division of Astronomical Sciences within the 
 Directorate for Mathematical and Physical Sciences, and the Division of Atmospheric and 
 Geospace Sciences within the Directorate for Geosciences.
 
 
====================================================================
 
© 2019 Dustin Kempton, Rafal Angryk
 
[Data Mining Lab](http://dmlab.cs.gsu.edu/), 
[Georgia State University](http://www.gsu.edu/)