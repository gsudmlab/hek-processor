# Listing of the hek-processor config file lines #

* [Return to READEME](./README.md)

##objectpool##
This set of values is for configuration of the datasource connection to the
database. We must have each of the sub values within the __objectpool__ tags.
Otherwise, your configuration will fail. They don't need to be in any particular
order though.

	<objectpool>
		<timeout value="6500" />
		<minpool value="2" />
		<maxpool value="5" />
		<maxsize value="10" />
		<username value="sdbuser" />
		<password value="sdbUserSP2019" />
		<validationquery value="SELECT 1;" />
		<driverclass value="org.postgresql.Driver" />
		<url value="jdbc:postgresql://localhost:5432/sdb_solardb​" />
	</objectpool>

###timeout###

	<timeout value="6500" />
	
Timeout value is how long the connection to the database can be idle before it
is considered to be timed out.

###minpool###

	<minpool value="2" />

The minimum number of simultaneous connections to the database that will be
kept alive regardless of traffic to the database.

###maxpool###

	<maxpool value="5" />

The maximum number of simultaneous connections that will be opened to the
database depending on traffic.

###username###

	<username value="sdbuser" />

The user name used to connect to the database. This is whatever you have
configured when you installed the database.

###password###

	<password value="sdbUserSP2019" />

This is the password used for the user above.

###validationquery###

	<validationquery value="SELECT 1;" />

This is the query sent to the database to verify that the connection is still
alive.

###driverclass###

	<driverclass value="org.postgresql.Driver" />

This is the namespace of the driver imported using the pom.xml dependencies (no
reason to change this unless the pom gets changed)

###url###

	<url value="jdbc:postgresql://localhost:5432/sdb_solardb​" />

This is the connection string used to connect to the database. It specifies the driver type, the host to connect to, the port to connect to, and the schema to connect to.

##epoc##

	<epoc date="2010-06-02 00:06:00" />
	
This the date time for the process to start processing from.  The data does not cover all of time, so we should not try to process it all.

##eventattribfile##

	<eventattribfile loc="tables" name="attributes.tsv"/>

This is to state the location of the file that contains the listing of each event type and what attributes the event type has.

##typeattribfile##

	<typeattribfile loc="tables" name="Parameter_Types.txt"/>

This is to state the location of the file that contains the listing of each attribute and what its type is, either string, or float, etc.  

##runcadence##

	<runcadence minutes="240" />

This is the number of minutes between each run of querying the datasource and inserting the new results into the database.

##threads##

	<threads max="4" />
	
This is the number of threads to use to process multiple event types/periods of time at the same time.

##blockingprocesses##

A list of processes that will block this process if they are currently working on producing their data.

	<blockingprocesses>
		<process name="" />
	</blockingprocesses>

###process###

	<process name="" />

This is a single element in the list of blocking processes. Use multiple if multiple processes block this one.

###Logger###

	<Logger loc=" " name = " " />

This is a the logger name and the location where the logger is present.

[Return to READEME](./README.md)