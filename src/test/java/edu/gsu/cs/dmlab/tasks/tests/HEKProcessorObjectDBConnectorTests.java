package edu.gsu.cs.dmlab.tasks.tests;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.slf4j.*;

import com.mysql.jdbc.Connection;
import edu.gsu.cs.dmlab.databases.HEKProcessorObjectDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBConnector;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.datatypes.interfaces.IISDEventReport;
import edu.gsu.cs.dmlab.exceptions.InvalidAttributeException;

class HEKProcessorObjectDBConnectorTests {

	@Test
	void test_NullDataSource() {
		DataSource dsourc = null;
		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_Nullmap() {
		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> map = null;
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_NullattributesTypemap() {
		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = null;

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_NullgeometryAttrributes() {
		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = null;
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_NullLogger() {
		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = null;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes, logger);
		});

	}

	/*--------------------CheckReportInDB-------------------------*/

	@Test
	void checkReportInDB_throwsSQLExceptionOnGetConnection() throws SQLException {

		DataSource dsourc = mock(DataSource.class);
		when(dsourc.getConnection()).thenThrow(new SQLException());

		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBConnector ob = new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes,
				logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.checkReportInDB(EventType.ACTIVE_REGION, " ");
		});

	}

	@Test
	void checkReportInDB_checkLogging() throws SQLException {

		DataSource dsourc = mock(DataSource.class);
		SQLException ex = new SQLException();
		when(dsourc.getConnection()).thenThrow(ex);

		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBConnector ob = new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes,
				logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.checkReportInDB(EventType.ACTIVE_REGION, " ");
		});

		ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<SQLException> exCaptor = ArgumentCaptor.forClass(SQLException.class);
		Mockito.verify(logger).error(stringCaptor.capture(), exCaptor.capture());
		assertEquals("SQL Exception while executing method checkReportInDB", stringCaptor.getValue());
		assertEquals(ex, exCaptor.getValue());

	}

	@Test
	void checkReportInDB_throwsSQLExceptionOnPreparedStatement() throws SQLException {

		Connection con = mock(Connection.class);
		// when(con.getSchema()).thenReturn(val);
		when(con.prepareStatement(any(String.class))).thenThrow(new SQLException());

		DataSource dsourc = mock(DataSource.class);
		when(dsourc.getConnection()).thenReturn(con);

		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBConnector ob = new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes,
				logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.checkReportInDB(EventType.ACTIVE_REGION, " ");
		});

	}

	/*-------------------insertEventReport---------------------------*/
	@Test
	void insertEventReport_throwsSQLExceptionOnGetConnection() throws SQLException, InvalidAttributeException {

		DataSource dsourc = mock(DataSource.class);
		when(dsourc.getConnection()).thenThrow(new SQLException());

		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);
		IISDEventReport eventReport = mock(IISDEventReport.class);

		IISDObjectDBConnector ob = new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes,
				logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.insertEventReport(eventReport);
		});

	}

	@Test
	void insertEventReport_checkLogging() throws SQLException, InvalidAttributeException {

		DataSource dsourc = mock(DataSource.class);
		SQLException ex = new SQLException();
		when(dsourc.getConnection()).thenThrow(ex);

		Map<EventType, Set<String>> map = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();

		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);
		IISDEventReport eventReport = mock(IISDEventReport.class);

		IISDObjectDBConnector ob = new HEKProcessorObjectDBConnector(dsourc, map, attributesTypeMap, geometryAttributes,
				logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.insertEventReport(eventReport);
		});

		ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<SQLException> exCaptor = ArgumentCaptor.forClass(SQLException.class);
		Mockito.verify(logger).error(stringCaptor.capture(), exCaptor.capture());
		assertEquals("SQL Exception while executing method insertEventReport", stringCaptor.getValue());
		assertEquals(ex, exCaptor.getValue());

	}

}
