package edu.gsu.cs.dmlab.tasks.tests;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import org.slf4j.*;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import edu.gsu.cs.dmlab.databases.HEKProcessorObjectDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBCreator;
import edu.gsu.cs.dmlab.datatypes.EventType;

class HEKProcessorObjectDBCreatorTests {

	@Test
	void test_NullDataSource() {

		DataSource dsourc = null;
		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_NulleventAttribMap() {

		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> eventAttribMap = null;
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_NullattributesTypeMap() {

		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = null;
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_NullgeometryAttributes() {

		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = null;
		Logger logger = mock(Logger.class);

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap, geometryAttributes, logger);
		});

	}

	@Test
	void test_NullLogger() {

		DataSource dsourc = mock(DataSource.class);
		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = null;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {

			new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap, geometryAttributes, logger);
		});

	}

	/*------------------------checkTableExists---------------------------*/
	@Test
	void checkTableExists_throwsSQLExceptionOnGetConnection() throws SQLException {

		DataSource dsourc = mock(DataSource.class);
		when(dsourc.getConnection()).thenThrow(new SQLException());

		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBCreator ob = new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap,
				geometryAttributes, logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.checkTableExists(EventType.ACTIVE_REGION);
		});

	}

	@Test
	void checkTableExists_checkLogging() throws SQLException {
		DataSource dsourc = mock(DataSource.class);
		SQLException ex = new SQLException();
		when(dsourc.getConnection()).thenThrow(ex);

		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBCreator ob = new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap,
				geometryAttributes, logger);
		try {
			ob.checkTableExists(EventType.ACTIVE_REGION);
		} catch (Exception e) {

		}
		ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<SQLException> exCaptor = ArgumentCaptor.forClass(SQLException.class);
		Mockito.verify(logger).error(stringCaptor.capture(), exCaptor.capture());
		assertEquals("SQL Exception while executing method checkTableExists", stringCaptor.getValue());
		assertEquals(ex, exCaptor.getValue());
	}

	@Test
	void checkTableExists_throwsSQLExceptionOnPreparedStatement() throws SQLException {

		Connection con = mock(Connection.class);
		when(con.getSchema()).thenReturn("");
		when(con.prepareStatement(any(String.class))).thenThrow(new SQLException());

		DataSource dsourc = mock(DataSource.class);
		when(dsourc.getConnection()).thenReturn(con);

		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBCreator ob = new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap,
				geometryAttributes, logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.checkTableExists(EventType.ACTIVE_REGION);
		});

	}

	@Test
	void checkTableExists_throwsSQLExceptionOnResultSet() throws SQLException {
		PreparedStatement val = mock(PreparedStatement.class);
		when(val.executeQuery()).thenThrow(new SQLException());

		Connection con = mock(Connection.class);
		when(con.getSchema()).thenReturn("");
		when(con.prepareStatement(any(String.class))).thenReturn(val);

		DataSource dsourc = mock(DataSource.class);
		when(dsourc.getConnection()).thenReturn(con);

		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBCreator ob = new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap,
				geometryAttributes, logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.checkTableExists(EventType.ACTIVE_REGION);
		});

	}

	/*--------------------------------createTable----------------------------*/

	@Test
	void createTable_throwsSQLExceptionOnGetConnection() throws SQLException {

		DataSource dsourc = mock(DataSource.class);
		when(dsourc.getConnection()).thenThrow(new SQLException());

		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBCreator ob = new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap,
				geometryAttributes, logger);
		Assertions.assertThrows(SQLException.class, () -> {
			ob.createTable(EventType.ACTIVE_REGION);
		});

	}

	@Test
	void createTable_checkLogging() throws SQLException {
		DataSource dsourc = mock(DataSource.class);
		SQLException ex = new SQLException();
		when(dsourc.getConnection()).thenThrow(ex);

		Map<EventType, Set<String>> eventAttribMap = new HashMap<EventType, Set<String>>();
		Map<String, String> attributesTypeMap = new HashMap<String, String>();
		Set<String> geometryAttributes = new HashSet<String>();
		Logger logger = mock(Logger.class);

		IISDObjectDBCreator ob = new HEKProcessorObjectDBCreator(dsourc, eventAttribMap, attributesTypeMap,
				geometryAttributes, logger);
		try {
			ob.createTable(EventType.ACTIVE_REGION);
		} catch (SQLException e) {

		}
		ArgumentCaptor<String> stringCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<SQLException> exCaptor = ArgumentCaptor.forClass(SQLException.class);
		Mockito.verify(logger).error(stringCaptor.capture(), exCaptor.capture());
		assertEquals("SQL Exception while executing method createTable", stringCaptor.getValue());
		assertEquals(ex, exCaptor.getValue());
	}

}
