package edu.gsu.cs.dmlab.tasks.tests;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBCreator;
import edu.gsu.cs.dmlab.datasources.interfaces.IISDEventDataSource;
import edu.gsu.cs.dmlab.datatypes.EventSaveRecord;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.datatypes.interfaces.IISDEventReport;
import edu.gsu.cs.dmlab.factories.interfaces.IObjectFactory;
import edu.gsu.cs.dmlab.tasks.FetchAndSaveEventsTask;

class FetchAndSaveTests {

	@Test
	void testNullObjectDBConnector() {
		IISDObjectDBConnector dbConnector = null;// mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = new DateTime();
		EventType type = EventType.ACTIVE_REGION;
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
					startTime, endTime, type);
		});
	}

	@Test
	void testNullObjectDBCreator() {
		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = null;// mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = new DateTime();
		EventType type = EventType.ACTIVE_REGION;
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
					startTime, endTime, type);
		});
	}

	@Test
	void testNullEventDataSourc() {
		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = null;// mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = new DateTime();
		EventType type = EventType.ACTIVE_REGION;
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
					startTime, endTime, type);
		});
	}

	@Test
	void testNullObjectFacory() {
		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = null; // mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = new DateTime();
		EventType type = EventType.ACTIVE_REGION;
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
					startTime, endTime, type);
		});
	}

	@Test
	void testNullStartTime() {
		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = null;// mock(DateTime.class);
		DateTime endTime = new DateTime();
		EventType type = EventType.ACTIVE_REGION;
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
					startTime, endTime, type);
		});
	}

	@Test
	void testNullEndTime() {
		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = null;// mock(DateTime.class);
		EventType type = EventType.ACTIVE_REGION;
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
					startTime, endTime, type);
		});
	}

	@Test
	void testNullEventType() {
		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = new DateTime();
		EventType type = null;// mock(ISDSolarEventType.class);
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
					startTime, endTime, type);
		});
	}

	@Test
	void testFetchPlus1DayLoop() throws Exception {

		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = startTime.plusDays(1);
		EventType type = EventType.ACTIVE_REGION;

		Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
				startTime, endTime, type);
		task.call();

		verify(dataSource, times(1)).getReports(any(DateTime.class), any(DateTime.class), eq(type));
	}

	@Test
	void testFetchPlus2DayLoop() throws Exception {

		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = startTime.plusDays(2);
		EventType type = EventType.ACTIVE_REGION;

		Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
				startTime, endTime, type);
		task.call();
		verify(dataSource, times(2)).getReports(any(DateTime.class), any(DateTime.class), eq(type));

	}

	@Test
	void testFetchPlus1Day1HourLoop() throws Exception {

		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = startTime.plusDays(1).plusHours(1);
		EventType type = EventType.ACTIVE_REGION;

		Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
				startTime, endTime, type);
		task.call();

		ArgumentCaptor<DateTime> captor = ArgumentCaptor.forClass(DateTime.class);
		verify(dataSource, times(2)).getReports(any(DateTime.class), captor.capture(), eq(type));

		List<DateTime> values = captor.getAllValues();
		assertTrue(values.contains(endTime));
	}

	@Test
	void testFetchPlus1HourNoLoop() throws Exception {

		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = startTime.plusHours(1);
		EventType type = EventType.ACTIVE_REGION;

		Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
				startTime, endTime, type);
		task.call();

		ArgumentCaptor<DateTime> captor = ArgumentCaptor.forClass(DateTime.class);
		verify(dataSource, times(1)).getReports(any(DateTime.class), captor.capture(), eq(type));

		List<DateTime> values = captor.getAllValues();
		assertTrue(values.contains(endTime));
	}

	@Test
	void testLastReportedPlus1HourNoLoop() throws Exception {

		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = startTime.plusHours(1);
		EventType type = EventType.ACTIVE_REGION;

		// Make list of events to return when datasource is asked for events
		List<IISDEventReport> resultList = new ArrayList<IISDEventReport>();
		// Tell report to return a date string in the yyyy-MM-dd'T'HH:mm:ss format.
		String dateString = "2012-01-02T12:12:12";
		IISDEventReport report = mock(IISDEventReport.class);
		when(report.getAttr(eq("event_starttime"))).thenReturn(dateString);
		resultList.add(report);

		// Tell the mock datasource to return the list we just made
		when(dataSource.getReports(any(DateTime.class), any(DateTime.class), any(EventType.class)))
				.thenReturn(resultList);

		// Tell the dbConnector to return false when checking if event in db
		when(dbConnector.checkReportInDB(any(EventType.class), any(String.class))).thenReturn(Boolean.FALSE);

		// Tell the dbConnector to return true when insert is called
		when(dbConnector.insertEventReport(any(IISDEventReport.class))).thenReturn(Boolean.TRUE);

		Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
				startTime, endTime, type);
		task.call();

		DateTimeFormatter Formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZoneUTC();
		DateTime returnedDate = Formatter.parseDateTime(dateString);
		verify(factory, times(1)).getEventSavedRecord(eq(EventType.ACTIVE_REGION), eq(returnedDate));

	}

	@Test
	void testLastReportedPlus1DayLoop() throws Exception {

		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = startTime.plusDays(1);
		EventType type = EventType.ACTIVE_REGION;

		// Make list of events to return when datasource is asked for events
		List<IISDEventReport> resultList = new ArrayList<IISDEventReport>();
		// Tell report to return a date string in the yyyy-MM-dd'T'HH:mm:ss format.
		String dateString = "2012-01-02T12:12:12";
		IISDEventReport report = mock(IISDEventReport.class);
		when(report.getAttr(eq("event_starttime"))).thenReturn(dateString);
		resultList.add(report);

		// Tell the mock datasource to return the list we just made
		when(dataSource.getReports(any(DateTime.class), any(DateTime.class), any(EventType.class)))
				.thenReturn(resultList);

		// Tell the dbConnector to return false when checking if event in db
		when(dbConnector.checkReportInDB(any(EventType.class), any(String.class))).thenReturn(Boolean.FALSE);

		// Tell the dbConnector to return true when insert is called
		when(dbConnector.insertEventReport(any(IISDEventReport.class))).thenReturn(Boolean.TRUE);

		Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
				startTime, endTime, type);
		task.call();

		DateTimeFormatter Formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZoneUTC();
		DateTime returnedDate = Formatter.parseDateTime(dateString);
		verify(factory, times(1)).getEventSavedRecord(eq(EventType.ACTIVE_REGION), eq(returnedDate));

	}

	@Test
	void testLastReportedPlus1Day1HourLoop() throws Exception {

		IISDObjectDBConnector dbConnector = mock(IISDObjectDBConnector.class);
		IISDObjectDBCreator dbCreator = mock(IISDObjectDBCreator.class);
		IISDEventDataSource dataSource = mock(IISDEventDataSource.class);
		IObjectFactory factory = mock(IObjectFactory.class);
		DateTime startTime = new DateTime();
		DateTime endTime = startTime.plusDays(1).plusHours(1);
		EventType type = EventType.ACTIVE_REGION;

		// Make list of events to return when datasource is asked for events
		List<IISDEventReport> resultList = new ArrayList<IISDEventReport>();
		// Tell report to return a date string in the yyyy-MM-dd'T'HH:mm:ss format.
		String dateString = "2012-01-02T12:12:12";
		String dateString2 = "2012-01-03T12:12:12";
		IISDEventReport report = mock(IISDEventReport.class);
		when(report.getAttr(eq("event_starttime"))).thenReturn(dateString).thenReturn(dateString2);
		resultList.add(report);

		// Tell the mock datasource to return the list we just made
		when(dataSource.getReports(any(DateTime.class), any(DateTime.class), any(EventType.class)))
				.thenReturn(resultList);

		// Tell the dbConnector to return false when checking if event in db
		when(dbConnector.checkReportInDB(any(EventType.class), any(String.class))).thenReturn(Boolean.FALSE);

		// Tell the dbConnector to return true when insert is called
		when(dbConnector.insertEventReport(any(IISDEventReport.class))).thenReturn(Boolean.TRUE);

		Callable<EventSaveRecord> task = new FetchAndSaveEventsTask(dbConnector, dbCreator, dataSource, factory,
				startTime, endTime, type);
		task.call();

		DateTimeFormatter Formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZoneUTC();
		DateTime returnedDate = Formatter.parseDateTime(dateString2);
		verify(factory, times(1)).getEventSavedRecord(eq(EventType.ACTIVE_REGION), eq(returnedDate));

	}

}
