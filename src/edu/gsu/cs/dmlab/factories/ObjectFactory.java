/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.factories;

import java.util.concurrent.Callable;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FutureCallback;
import com.google.gson.JsonObject;

import edu.gsu.cs.dmlab.config.ConfigReader;
import edu.gsu.cs.dmlab.databases.HEKProcessorObjectDBConnector;
import edu.gsu.cs.dmlab.databases.HEKProcessorObjectDBCreator;
import edu.gsu.cs.dmlab.databases.Postgres_StateDBConnector;
import edu.gsu.cs.dmlab.databases.Postgres_StateDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBCreator;
import edu.gsu.cs.dmlab.datasources.interfaces.IISDEventDataSource;
import edu.gsu.cs.dmlab.datatypes.EventSaveRecord;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.datatypes.ISDSolarEventReport;
import edu.gsu.cs.dmlab.datatypes.interfaces.IISDEventReport;
import edu.gsu.cs.dmlab.factories.interfaces.IObjectFactory;
import edu.gsu.cs.dmlab.factory.interfaces.IHEKObjectFactory;
import edu.gsu.cs.dmlab.services.interfaces.IWorkerTaskSupervisor;
import edu.gsu.cs.dmlab.tasks.EventsSavedCallback;
import edu.gsu.cs.dmlab.tasks.FetchAndSaveEventsTask;
import edu.gsu.cs.gsu.datasources.HekISDEventDataSource;

/**
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ObjectFactory implements IObjectFactory, IHEKObjectFactory {

	private IISDObjectDBCreator objDBCreator;
	private IISDObjectDBConnector dbConnector;
	private IISDEventDataSource eventDataSource;
	private IISDStateDBCreator stateDBCreator;
	private IISDStateDBConnector stateDBConnector; // Interface

	public ObjectFactory(ConfigReader config) {
		if (config == null)
			throw new IllegalArgumentException("ConfigReader cannot be null in ObjectFactory constructor.");

		this.objDBCreator = new HEKProcessorObjectDBCreator(config.getObjDBPoolSource(), config.getEventAttributeMap(),
				config.getAttributesTypeMap(), config.getGeometryAttributes(), null);
		this.eventDataSource = new HekISDEventDataSource(this);
		Logger sdbCreatorLogg = LoggerFactory.getLogger(Postgres_StateDBCreator.class);
		this.stateDBCreator = new Postgres_StateDBCreator(config.getObjDBPoolSource(), sdbCreatorLogg);
		Logger sdbConnectorLogg = LoggerFactory.getLogger(Postgres_StateDBConnector.class);
		this.stateDBConnector = new Postgres_StateDBConnector(config.getObjDBPoolSource(),
				config.getBlockingProcesses(), config.getEpoc(), config.getProcessName(), sdbConnectorLogg);

		this.dbConnector = new HEKProcessorObjectDBConnector(config.getObjDBPoolSource(), config.getEventAttributeMap(),
				config.getAttributesTypeMap(), config.getEventAttributeStatusMap(), config.getGeometryAttributes(),
				null);
	}

	@Override
	public IISDEventReport getEventReportFromJson(JsonObject jsonInput) throws IllegalArgumentException {
		return new ISDSolarEventReport(jsonInput);
	}

	@Override
	public IISDObjectDBCreator getDBCreator() {
		return this.objDBCreator;
	}

	@Override
	public IISDStateDBCreator getStateDBCreator() {
		return this.stateDBCreator;
	}

	@Override
	public IISDStateDBConnector getStateDBConnector() {
		return this.stateDBConnector;
	}

	public Callable<EventSaveRecord> getFetchAndSaveTask(EventType type, DateTime startTime, DateTime endTime) {
		return new FetchAndSaveEventsTask(this.dbConnector, this.objDBCreator, this.eventDataSource, this, startTime,
				endTime, type);
	}

	@Override
	public FutureCallback<EventSaveRecord> getSavedCallback(IWorkerTaskSupervisor supervisor) {
		return new EventsSavedCallback(supervisor);
	}

	@Override
	public EventSaveRecord getEventSavedRecord(EventType type, DateTime lastInsertedTime) {
		EventSaveRecord record = new EventSaveRecord();
		record.type = type;
		record.lastInsertedTime = lastInsertedTime;
		return record;
	}

}
