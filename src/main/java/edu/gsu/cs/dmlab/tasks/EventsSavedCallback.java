/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.google.common.util.concurrent.FutureCallback;

import edu.gsu.cs.dmlab.datatypes.EventSaveRecord;
import edu.gsu.cs.dmlab.services.interfaces.IWorkerTaskSupervisor;

/**
 * The class that will perform the callback for download and then insert
 * operations.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class EventsSavedCallback implements FutureCallback<EventSaveRecord> {

	private IWorkerTaskSupervisor supervisor;

	public EventsSavedCallback(IWorkerTaskSupervisor supervisor) {
		this.supervisor = supervisor;
	}

	@Override
	public void onFailure(Throwable arg0) {
		this.supervisor.handleEventsSavedFailed(arg0);
	}

	@Override
	public void onSuccess(@Nullable EventSaveRecord record) {
		this.supervisor.handleEventsSaved(record);
	}

}
