/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import java.util.List;
import java.util.concurrent.Callable;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBCreator;
import edu.gsu.cs.dmlab.datasources.interfaces.IISDEventDataSource;
import edu.gsu.cs.dmlab.datatypes.EventSaveRecord;
import edu.gsu.cs.dmlab.datatypes.EventType;

import edu.gsu.cs.dmlab.datatypes.interfaces.IISDEventReport;
import edu.gsu.cs.dmlab.factories.interfaces.IObjectFactory;

/**
 * The class that will perform the call for download and then insert operations.
 * This class shall be scheduled as a task, which returns the type that was
 * inserted and the last start date and time of an object that was processed.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class FetchAndSaveEventsTask implements Callable<EventSaveRecord> {

	private static final DateTimeFormatter Formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZoneUTC();

	private IISDObjectDBConnector dbConnector;
	private IISDEventDataSource dataSource;
	private DateTime startTime;
	private DateTime endTime;
	private EventType type;
	private IObjectFactory factory;

	/**
	 * The constructor for the download and save processing task class.
	 * 
	 * @param dbConnector The object database connector used to test if an object is
	 *                    already in the underlying database.
	 * 
	 * @param dbCreator   The object database creator used to insert an object into
	 *                    the underlying database.
	 * 
	 * @param dataSource  The object datasource that pulls the objects from some
	 *                    remote location.
	 * 
	 * @param factory     The object factory used to produce various objects used in
	 *                    this class.
	 * 
	 * @param startTime   The start date and time for this task to begin its
	 *                    processing from.
	 * 
	 * @param endTime     The end date and time for this task to stop processing at.
	 * 
	 * @param type        The type of event for this processing task to work with.
	 */
	public FetchAndSaveEventsTask(IISDObjectDBConnector dbConnector, IISDObjectDBCreator dbCreator,
			IISDEventDataSource dataSource, IObjectFactory factory, DateTime startTime, DateTime endTime,
			EventType type) {
		if (dbConnector == null)
			throw new IllegalArgumentException(
					"IISDObjectDBConnector cannot be null in FetchAndSaveEventsTask constructor.");
		if (dbCreator == null)
			throw new IllegalArgumentException(
					"IISDObjectDBCreator cannot be null in FetchAndSaveEventsTask constructor.");
		if (dataSource == null)
			throw new IllegalArgumentException(
					"IISDEventDataSource cannot be null in FetchAndSaveEventsTask constructor.");
		if (factory == null)
			throw new IllegalArgumentException("IObjectFactory cannot be null in FetchAndSaveEventsTask constructor.");
		if (startTime == null)
			throw new IllegalArgumentException(
					"DateTime startTime cannot be null in FetchAndSaveEventsTask constructor.");
		if (endTime == null)
			throw new IllegalArgumentException(
					"DateTime endTime cannot be null in FetchAndSaveEventsTask constructor.");
		if (type == null)
			throw new IllegalArgumentException(
					"IISDSolarEventType cannot be null in FetchAndSaveEventsTask constructor.");

		this.dataSource = dataSource;
		this.dbConnector = dbConnector;
		this.factory = factory;
		this.startTime = startTime;
		this.endTime = endTime;
		this.type = type;
	}

	/**
	 * Call instructs the task to begin processing.
	 * 
	 * @return An indicator of the last start date and time of object processed and
	 *         what type of events this task was processing.
	 * 
	 * @throws Exception If any number of errors happen while processing. Such as
	 *                   connections to databases being interrupted, either the
	 *                   storage location or the source location, etc.
	 */
	@Override
	public EventSaveRecord call() throws Exception {

		// Get a local start that we will increment by a day if necessary
		DateTime localStart = new DateTime(this.startTime);

		// Calculate the number of days between the start and end of the range to
		// process
		int numDays = Days.daysBetween(this.startTime, this.endTime).getDays();

		// A holder for the last datetime
		DateTime lastInserted = null;

		// If multiple days of data is to be processed, we will process one day at a
		// time.
		while (numDays > 0) {
			DateTime tmpEnd = localStart.plusDays(1);
			// Gets the list of events to insert based on the informaiton passed in.
			List<IISDEventReport> events = this.dataSource.getReports(localStart, tmpEnd, this.type);

			// Insert the events that were pulled from the datasource.
			for (IISDEventReport event : events) {
				if (!this.dbConnector.checkReportInDB(this.type, event.getAttr("kb_archivid"))) {
					if (this.dbConnector.insertEventReport(event))
						lastInserted = Formatter.parseDateTime(event.getAttr("event_starttime"));
				}
			}
			localStart = tmpEnd;
			numDays = Days.daysBetween(localStart, this.endTime).getDays();
		}

		// Process the last partial day of data to be downloaded and inserted.
		if (this.endTime.isAfter(localStart)) {
			List<IISDEventReport> events = this.dataSource.getReports(localStart, this.endTime, this.type);

			// Insert the events that were pulled from the datasource.
			for (IISDEventReport event : events) {
				if (!this.dbConnector.checkReportInDB(this.type, event.getAttr("kb_archivid"))) {
					if (this.dbConnector.insertEventReport(event))
						lastInserted = Formatter.parseDateTime(event.getAttr("event_starttime"));

				}
			}
		}

		// Returns the information about the last inserted event, as well as what was
		// being processed here.
		return this.factory.getEventSavedRecord(this.type, lastInserted);
	}

}
