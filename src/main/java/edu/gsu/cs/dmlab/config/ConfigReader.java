/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.io.IoBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;

/**
 * The configuration file reader class. This class reads the configuration file
 * for this project and provides the objects needed for this project that can be
 * configured at runtime through file changes.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ConfigReader {

	/* CLASS FIELDS */
	private DataSource objDBPoolSourc = null;
	private DateTime epoc;
	private Map<EventType, Set<String>> eventAttribMap;
	private Map<String, String> attributesTypeMap;
	private List<String> blockingProcesses;
	private List<EventType> configuredEventList;
	private DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
	private int runCadece;
	private int maxThreads;
	private String processName;

	/*
	 * A filter map to convert the retrieved types into the postgres types.
	 */
	private static final Map<String, String> attributeTypeFilter = new HashMap<>();
	static {
		attributeTypeFilter.put("float", "float");
		attributeTypeFilter.put("string", "text");
		attributeTypeFilter.put("integer", "integer");
		attributeTypeFilter.put("long", "bigint");
		attributeTypeFilter.put("null", "text");
		attributeTypeFilter.put("timestamp", "timestamp");
		attributeTypeFilter.put("point", "text");
		attributeTypeFilter.put("polygon", "text");
		attributeTypeFilter.put("serial", "serial");
	}

	private static final Set<String> geometryAttributes = new HashSet<>();
	static {
		geometryAttributes.add("hgc_bbox");
		geometryAttributes.add("hgc_coord");
		geometryAttributes.add("hgs_bbox");
		geometryAttributes.add("hgs_coord");
		geometryAttributes.add("hpc_bbox");
		geometryAttributes.add("hpc_coord");
		geometryAttributes.add("hrc_bbox");
		geometryAttributes.add("hrc_coord");
		geometryAttributes.add("hgc_boundcc");
		geometryAttributes.add("hgc_boundcc");
		geometryAttributes.add("hgs_boundcc");
		geometryAttributes.add("hpc_boundcc");
		geometryAttributes.add("hrc_boundcc");
		geometryAttributes.add("bound_chaincode");
		// only these two will be actual spatial objects in the DB
		geometryAttributes.add("hpc_bbox_spatialobj");
		geometryAttributes.add("hpc_boundcc_spatialobj");
	}

	/* CLASS CONSTRUCTOR */
	public ConfigReader(String fileLoc, String configName) throws InvalidConfigException {

		this.config(fileLoc, configName);
	}

	/* GETTERS & SETTERS */
	public DataSource getObjDBPoolSource() {
		return this.objDBPoolSourc;
	}

	public DateTime getEpoc() {
		return this.epoc;
	}

	public Map<EventType, Set<String>> getEventAttributeMap() {
		return this.eventAttribMap;
	}

	public Set<String> getGeometryAttributes() {
		return geometryAttributes;
	}

	public Map<String, String> getAttributesTypeMap() {
		return this.attributesTypeMap;
	}

	public List<String> getBlockingProcesses() {
		return this.blockingProcesses;
	}

	public int getRunCadence() {
		return this.runCadece;
	}

	public int getMaxThreads() {
		return this.maxThreads;
	}

	public String getProcessName() {
		return this.processName;
	}

	public List<EventType> getConfiguredTypes() {
		return this.configuredEventList;
	}

	/* PRIVATE METHODS */
	private void config(String folderLocation, String configName) throws InvalidConfigException {
		try {

			DocumentBuilderFactory fctry = DocumentBuilderFactory.newInstance();
			Document doc;
			String fileLoc = folderLocation + File.separator + configName;
			DocumentBuilder bldr = fctry.newDocumentBuilder();
			doc = bldr.parse(new File(fileLoc));
			doc.getDocumentElement().normalize();

			// Used to hold the config for the object pool until after logging config is
			// completed.
			Node objPoolNode = null;

			Element root = doc.getDocumentElement();
			NodeList ndLst = root.getChildNodes();

			for (int i = 0; i < ndLst.getLength(); i++) {
				Node nde = ndLst.item(i);
				if (nde.getNodeType() == Node.ELEMENT_NODE) {
					String ndName = nde.getNodeName();
					switch (ndName) {
					case "objectpool":
						objPoolNode = nde;
						break;
					case "epoc":
						this.epoc = formatter.parseDateTime(this.getAttrib(nde, "date"));
						break;
					case "eventattribfile":
						folderLocation = this.getAttrib(nde, "loc");
						String fileName = this.getAttrib(nde, "name");
						String attribFileLoc = folderLocation + File.separator + fileName;
						this.eventAttribMap = this.getEventAttributeMap(attribFileLoc);
						this.attributesTypeMap = this.getAttributeTypeMap(attribFileLoc);
						break;
					case "runcadence":
						this.runCadece = Integer.parseInt(this.getAttrib(nde, "minutes"));
						break;
					case "threads":
						this.maxThreads = Integer.parseInt(this.getAttrib(nde, "max"));
						break;
					case "blockingprocesses":
						this.blockingProcesses = this.getBlockingProcessesList(nde.getChildNodes());
						break;
					case "process_name":
						this.processName = this.getAttrib(nde, "name");
						break;
					case "logger":
						folderLocation = this.getAttrib(nde, "loc");
						String logsFileName = this.getAttrib(nde, "name");
						logsFileName = folderLocation + File.separator + logsFileName;
						this.setupLogging(logsFileName);
						break;
					case "configuredevents":
						this.configuredEventList = this.getConfiguredEventsList(nde.getChildNodes());
						break;
					default:
						System.out.print("Unknown Element: ");
						System.out.println(ndName);
					}

				}
			}

			if (objPoolNode != null) {
				this.objDBPoolSourc = this.getPoolSourc(objPoolNode.getChildNodes());
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidConfigException("Config failed with: " + e.getMessage());
		}

	}

	private void setupLogging(String logConfFile) {
		Properties props = System.getProperties();
		props.setProperty("log4j.configurationFile", logConfFile);
		File f = new File(logConfFile);
		URI fc = f.toURI();
		LoggerContext.getContext().setConfigLocation(fc);
	}

	private List<String> getBlockingProcessesList(NodeList ndLst) {
		List<String> blockingList = new ArrayList<String>();
		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {

				case "process":
					String blockingProcess = this.getAttrib(nde, "name");
					blockingList.add(blockingProcess);
					break;
				default:
					System.out.print("Unknown Element: ");
					System.out.println(ndName);
				}
			}
		}
		return blockingList;
	}

	private List<EventType> getConfiguredEventsList(NodeList ndLst) {
		List<EventType> evList = new ArrayList<EventType>();
		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {

				case "event":
					String ev = this.getAttrib(nde, "name");
					evList.add(EventType.fromString(ev));
					break;
				default:
					System.out.print("Unknown Element: ");
					System.out.println(ndName);
				}
			}
		}
		return evList;
	}

	/**
	 * Reads the csv file tagged as "eventattribfile" in the config file, (which is
	 * currently set to be "attributes.csv"), and returns a map of the pairs of
	 * 'parameters' and 'types'. The types are filtered using 'attributeTypeFilter'
	 * to match the Postgres data types.
	 * 
	 * @param fileLoc
	 * @return
	 */
	private Map<EventType, Set<String>> getEventAttributeMap(String fileLoc) {
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(new File(fileLoc))))) {

			Map<EventType, Set<String>> eventAttributeMap = new TreeMap<>();
			String[][] all = new String[241][31];
			String line;
			int count = 0;
			while ((line = reader.readLine()) != null) {
				String[] values = line.split("\t");
				all[count] = values;
				count++;
			}

			for (int i = 2; i < all[0].length - 1; i++) {
				Set<String> attributesSpecificToEvent = new HashSet<>();
				for (int k = 1; k < all.length; k++) {
					if (all[k][i].equalsIgnoreCase("opt") || all[k][i].equalsIgnoreCase("req")) {
						attributesSpecificToEvent.add(all[k][0].toLowerCase());
					}
				}
				eventAttributeMap.put(EventType.fromString(all[0][i]), attributesSpecificToEvent);
			}
			return eventAttributeMap;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Reads the csv file tagged as "typeattribfile" in the config file, (which is
	 * currently set to be "Parameter_Types.csv"), and returns a map of the pairs of
	 * 'parameters' and 'types'. The types are filtered using 'attributeTypeFilter'
	 * to match the Postgres data types.
	 * 
	 * @param attrFilePath
	 * @return
	 */
	private Map<String, String> getAttributeTypeMap(String attrFilePath) {

		Map<String, String> attributeTypeMap = new HashMap<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(attrFilePath))) {

			// Skip first line - header
			String line = reader.readLine();
			while ((line = reader.readLine()) != null) {
				String[] columns = line.split("\t");
				String attrib = columns[0];
				String attrType = columns[1];

				// Convert the type to its postgres type
				attrType = attributeTypeFilter.get(attrType);
				attributeTypeMap.put(attrib, attrType);
			}

			return attributeTypeMap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return attributeTypeMap;
	}

	private DataSource getPoolSourc(NodeList ndLst) {

		BasicDataSource dbPoolSourc = null;

		dbPoolSourc = new BasicDataSource();
		PrintWriter logger = IoBuilder.forLogger(BasicDataSource.class).setLevel(Level.INFO).buildPrintWriter();

		dbPoolSourc.setPoolPreparedStatements(false);
		dbPoolSourc.setDefaultAutoCommit(true);

		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {

				case "timeout":
					String idlStr = this.getAttrib(nde, "value");
					dbPoolSourc.setDefaultQueryTimeout(Integer.parseInt(idlStr));
					dbPoolSourc.setSoftMinEvictableIdleTimeMillis(Integer.parseInt(idlStr));
					break;
				case "minpool":
					String minStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMinIdle(Integer.parseInt(minStr));
					break;
				case "maxpool":
					String maxStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxIdle(Integer.parseInt(maxStr));
					break;
				case "maxsize":
					String maxszStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxTotal(Integer.parseInt(maxszStr));
					break;
				case "username":
					dbPoolSourc.setUsername(this.getAttrib(nde, "value"));
					break;
				case "password":
					dbPoolSourc.setPassword(this.getAttrib(nde, "value"));
					break;
				case "validationquery":
					dbPoolSourc.setValidationQuery(this.getAttrib(nde, "value"));
					break;
				case "driverclass":
					dbPoolSourc.setDriverClassName(this.getAttrib(nde, "value"));
					break;
				case "url":
					String connectUrl = this.getAttrib(nde, "value");
					dbPoolSourc.setUrl(connectUrl);
					break;
				case "schema":
					String schema = this.getAttrib(nde, "value");
					dbPoolSourc.setDefaultSchema(schema);
					break;
				default:
					System.out.print("Unknown Element: ");
					System.out.println(ndName);
				}
			}
		}

		try {
			dbPoolSourc.setLogWriter(logger);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return dbPoolSourc;
	}

	private String getAttrib(Node prntNde, String attName) {
		StringBuffer buf = new StringBuffer();
		boolean isSet = false;
		if (prntNde.hasAttributes()) {
			NamedNodeMap ndeMp = prntNde.getAttributes();
			for (int i = 0; i < ndeMp.getLength(); i++) {
				Node nde = ndeMp.item(i);
				if (nde.getNodeName().compareTo(attName) == 0) {
					buf.append(nde.getNodeValue());
					isSet = true;
					break;
				}
			}
		}

		if (!isSet) {
			return "";
		} else {
			return buf.toString();
		}
	}

}
