/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.services;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.core.LoggerContext;
import org.joda.time.DateTime;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.gsu.cs.dmlab.config.ConfigReader;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBCreator;
import edu.gsu.cs.dmlab.datatypes.EventSaveRecord;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.factories.ObjectFactory;
import edu.gsu.cs.dmlab.factories.interfaces.IObjectFactory;
import edu.gsu.cs.dmlab.services.interfaces.IWorkerTaskSupervisor;

/**
 * The class that will control all of the actions of the project. It will run a
 * new set of download and save tasks based on the input from the configuration
 * file.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class WorkerTaskSupervisor implements IWorkerTaskSupervisor {

	private long runCadenceMilis;
	private ListeningExecutorService comExecutor;
	private ListeningExecutorService callbackExecutor;
	private IObjectFactory factory;
	private IISDStateDBConnector stateDb;

	private LinkedList<Future<EventSaveRecord>> fetchingTaskList;

	private List<EventType> types;

	private Lock lock;
	private Logger logger;
	private DateTime configEpoc;

	public WorkerTaskSupervisor(ConfigReader config) throws Exception {

		this.logger = LoggerFactory.getLogger(WorkerTaskSupervisor.class);

		int maxThreads = config.getMaxThreads();
		this.comExecutor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(maxThreads));
		this.callbackExecutor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(maxThreads));
		this.runCadenceMilis = config.getRunCadence() * 60 * 1000;

		this.factory = new ObjectFactory(config);

		// Check if the state table is present and create if it is not.
		IISDStateDBCreator stateDBCreator = this.factory.getStateDBCreator();
		if (!stateDBCreator.checkStateTableExists())
			stateDBCreator.createStateTable();
		this.stateDb = this.factory.getStateDBConnector();

		this.fetchingTaskList = new LinkedList<Future<EventSaveRecord>>();

		// Check if each of the event type tables are present and create if they are
		// not.
		IISDObjectDBCreator objDB = this.factory.getDBCreator();
		this.configEpoc = config.getEpoc();
		this.types = config.getConfiguredTypes();

		// First check for the tables to hold each configured event type
		this.types.forEach(type -> {
			try {
				if (!objDB.checkTableExists(type))
					objDB.createTable(type);
			} catch (SQLException e) {
				this.logger.error("Error while checking for event table " + type.toString(), e);
			}
		});

		// Check state table exists and set event type as not processing.
		// This assumes that there shall be only one of this process running at any
		// point in time, and if the is processing flag is set that was caused by the
		// previous execution of this process being killed prematurely.
		this.types.forEach(type -> {
			try {
				this.stateDb.setFinishedProcessing(type);
			} catch (SQLException | IllegalAccessException e) {
				this.logger.error("Error while checking state table for event type " + type.toString(), e);
			}
		});

		this.lock = new ReentrantLock();
	}

	/**
	 * This is the main run method, it will keep running for as long as this program
	 * is not killed.
	 */
	public void run() {
		while (true) {
			// We want to run indefinitely, so we just loop forever.
			// The addTask method should block for some period of time between runs.
			this.addTask();
		}
	}

	private void addTask() {

		this.lock.lock();
		try {
			this.types.forEach(type -> {

				try {
					// Check if we can process this event type, if not, we just skip for now.

					if (this.stateDb.checkOKToProcess(type)) {

						// Set that we are processing.
						this.stateDb.setIsProcessing(type);

						// Get the last date and time of the last inserted object.
						DateTime startTime = this.stateDb.getLastProcessedTime(type);
						if (startTime == null)
							startTime = new DateTime(this.configEpoc);

						DateTime endTime = new DateTime();

						// Create a fetch and save task then submit it to the communication executer
						Callable<EventSaveRecord> fetchTask = this.factory.getFetchAndSaveTask(type, startTime,
								endTime);
						ListenableFuture<EventSaveRecord> retrievalFutureTask = (ListenableFuture<EventSaveRecord>) this.comExecutor
								.submit(fetchTask);

						// Create a callback task for the fetch and save task we just created and submit
						// it to the communication executor. The callback is just a wrapper to call back
						// to the methods in this class that are for handling the events having been
						// fetched and saved.
						FutureCallback<EventSaveRecord> savedCallback = this.factory.getSavedCallback(this);
						Futures.addCallback(retrievalFutureTask, savedCallback, this.callbackExecutor);

						// Finally add the fetch and save task to a list so we can keep track of how
						// many tasks are currently in process (not really using this at the moment but
						// keeping it around in case we want it in the future).
						this.fetchingTaskList.add(retrievalFutureTask);
					}
				} catch (SQLException e) {
					// Do nothing, we've already logged the SQLException
					e.printStackTrace();
				} catch (Exception e) {
					this.logger.error("Exception while executing addTask method", e);
				}
			});
		} finally {
			this.lock.unlock();
		}

		// At the end of it all, we want to wait for a period of time before running
		// again.
		try {
			Thread.sleep(this.runCadenceMilis);
		} catch (InterruptedException e) {
			this.logger.error("Interrupted Exception occured ", e);
		}
	}

	@Override
	public void handleEventsSaved(EventSaveRecord record) {
		this.lock.lock();
		try {
			// Cycle through the list of tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We currently aren't even using this information, but it
			// has been useful in other projects, so I am putting it in this project for
			// possible future use.
			Iterator<Future<EventSaveRecord>> iter = this.fetchingTaskList.iterator();
			while (iter.hasNext()) {
				Future<EventSaveRecord> tsk = iter.next();
				if (tsk.isDone()) {
					iter.remove();
				}
			}

			if (record != null) {
				if (record.lastInsertedTime != null)
					this.stateDb.updateLastProcessedTime(record.type, record.lastInsertedTime);
				this.stateDb.setFinishedProcessing(record.type);
				this.logger.info("Record Type {}, Last Inserted Time {}", record.type, record.lastInsertedTime);
			}
		} catch (Exception e) {
			this.logger.error("Exception in method handleEventsSaved", e);
		} finally {
			this.lock.unlock();
		}
	}

	@Override
	public void handleEventsSavedFailed(Throwable arg0) {

		logger.error("Failed to Save! ", arg0);
		this.lock.lock();
		try {
			// Cycle through the list of tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We currently aren't even using this information, but it
			// has been useful in other projects, so I am putting it in this project for
			// possible future use.
			Iterator<Future<EventSaveRecord>> iter = this.fetchingTaskList.iterator();
			while (iter.hasNext()) {
				Future<EventSaveRecord> tsk = iter.next();
				if (tsk.isDone()) {
					iter.remove();
				}
			}

		} finally {
			this.lock.unlock();
		}

	}
}
