/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.services.interfaces;

import edu.gsu.cs.dmlab.datatypes.EventSaveRecord;

/**
 * The public interface for the worker supervisor class. The methods listed here
 * are meant to be callback methods for various tasks that will be run. That way
 * the logic of the program can come back to the implementing class after each
 * of the tasks are complete.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface IWorkerTaskSupervisor {

	/**
	 * The method used to handle when a fetch and save task has completed.
	 * 
	 * @param record
	 *            A record that indicates what the last event start time was that
	 *            was processed and what event type it was.
	 */
	public void handleEventsSaved(EventSaveRecord record);

	/**
	 * The method used to handle when a fetch and save task has thrown an error.
	 * 
	 * @param arg0
	 *            The exception that was thrown, if the implementing class wants to
	 *            do any sort of error handling based on what type it is.
	 */
	public void handleEventsSavedFailed(Throwable arg0);
}
