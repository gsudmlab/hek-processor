/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.factories.interfaces;

import java.util.concurrent.Callable;

import org.joda.time.DateTime;

import com.google.common.util.concurrent.FutureCallback;

import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBCreator;

import edu.gsu.cs.dmlab.datatypes.EventSaveRecord;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.services.interfaces.IWorkerTaskSupervisor;

/**
 * Contain the object building methods for objects defined in this project.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface IObjectFactory {

	/**
	 * Method to get an object that stores the type and time of a recored that was
	 * saved.
	 * 
	 * @param type             Type of event that was processed.
	 * 
	 * @param lastInsertedTime The timestamp of the object that was saved
	 * 
	 * @return Object that stores the type and timestamp of a saved recored.
	 */
	public EventSaveRecord getEventSavedRecord(EventType type, DateTime lastInsertedTime);

	/**
	 * Gets the database connection object used to create the object database
	 * tables.
	 * 
	 * @return The database connection object used to create the object database
	 *         tables.
	 */
	public IISDObjectDBCreator getDBCreator();

	/**
	 * Gets the database connection object used to create the state table in the
	 * database.
	 * 
	 * @return The database connection object used to create the state table in the
	 *         database.
	 */
	public IISDStateDBCreator getStateDBCreator();

	/**
	 * Gets the database connection object used to update the state table in the
	 * database.
	 * 
	 * @return The database connection object used to update the state table in the
	 *         database.
	 */
	public IISDStateDBConnector getStateDBConnector();

	/**
	 * Gets a task object that both fetches objects of a specific type from a
	 * datasource and saves objects in the local database.
	 * 
	 * @param type      The type of event to fetch and save.
	 * @param startTime The start of the period to process
	 * @param endTime   The end of the period to process
	 * @return A callable task object that both fetches objects of a specific type
	 *         from a datasource and saves objects in the local database.
	 */
	public Callable<EventSaveRecord> getFetchAndSaveTask(EventType type, DateTime startTime, DateTime endTime);

	/**
	 * Gets a callback object used as a callback when a future task completes
	 * processing.
	 * 
	 * @param supervisor The supervisor wishing to be called when the future task
	 *                   completes processing
	 * @return A callback object used as a callback when a future task completes
	 *         processing.
	 */
	public FutureCallback<EventSaveRecord> getSavedCallback(IWorkerTaskSupervisor supervisor);
}
