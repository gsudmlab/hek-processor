/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.databases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;
import org.slf4j.*;
//import  org.apache.logging.log4j.Logger;
import javax.sql.DataSource;
import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBCreator;
import edu.gsu.cs.dmlab.datatypes.EventType;

/**
 * 
 * @author Azim Ahmadzadeh and Surabhi Priya, Data Mining Lab, Georgia State
 *         University
 *
 */
public class HEKProcessorObjectDBCreator implements IISDObjectDBCreator {

	private final Logger logger;

	/*
	 * Map of the Names of all the geometry attributes to be used on all the event
	 * types.
	 */
	private Set<String> geometryAttributes;

	private DataSource dsourc; // Data source connection to the database

	/*
	 * Map of the attribute names for each event type
	 */
	private Map<EventType, Set<String>> eventAttribMap;

	/*
	 * Map of <Attribute Name, Attribute type> for the attributes listed in the
	 * above map.
	 */
	private Map<String, String> attributesTypeMap;

	/**
	 * Constructor of the object database creator object.
	 * 
	 * @param dsourc             The datasource connection object used to connect to
	 *                           the database.
	 * 
	 * @param eventAttribMap     Map of the attribute names for each event type
	 * 
	 * @param attributesTypeMap  Map of <Attribute Name, Attribute type> for the
	 *                           attributes listed in the above map.
	 * 
	 * @param geometryAttributes Names of all the geometry attributes to be used on
	 *                           all the event types.
	 * 
	 * @param logger             The SLF4J logging object used to log errors that
	 *                           occur in this object.
	 */
	public HEKProcessorObjectDBCreator(DataSource dsourc, Map<EventType, Set<String>> eventAttribMap,
			Map<String, String> attributesTypeMap, Set<String> geometryAttributes, Logger logger) {

		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in HEKProcessorDBCreator constructor.");
		if (eventAttribMap == null)
			throw new IllegalArgumentException(
					"Event attribute map cannot be null in HEKProcessorDBCreator constructor.");
		if (attributesTypeMap == null)
			throw new IllegalArgumentException(
					"Attribute type map cannot be null in HEKProcessorDBCreator constructor.");

		if (geometryAttributes == null)
			throw new IllegalArgumentException(
					"Geometry Attribute type map cannot be null in HEKProcessorDBCreator constructor.");
		if (logger == null)
			throw new IllegalArgumentException("logger cannot be null in HEKProcessorDBCreator constructor.");

		this.dsourc = dsourc;
		this.eventAttribMap = eventAttribMap;
		this.attributesTypeMap = attributesTypeMap;
		this.geometryAttributes = geometryAttributes;
		this.logger = logger;
	}

	public boolean checkTableExists(EventType type) throws SQLException {
		boolean exists = false;
		try (Connection con = this.dsourc.getConnection()) {
			con.setAutoCommit(true);
			String schema = con.getSchema();

			// Check for the table and create if not there.
			String query = this.generateQuery_eventTableExists(type, schema);
			try (PreparedStatement tableExistsPrepStmt = con.prepareStatement(query)) {
				try (ResultSet res = tableExistsPrepStmt.executeQuery()) {
					if (res.next()) {
						exists = res.getBoolean(1);
					}
				}
			}
		} catch (SQLException e) {
			logger.error("SQL Exception while executing method checkTableExists", e);
			throw e;
		}
		return exists;
	}

	@Override
	public boolean createTable(EventType type) throws SQLException {

		String query = "";
		try (Connection con = this.dsourc.getConnection()) {
			con.setAutoCommit(true);
			String schema = con.getSchema();
			if (!this.checkTableExists(type)) {
				query = this.generateQuery_createTable(type, schema);
				try (PreparedStatement createTableStatement = con.prepareStatement(query)) {
					if (createTableStatement.executeUpdate() == 0) {
						return true;
					}
				}
			}
		} catch (SQLException e) {
			logger.error("SQL Exception while executing method createTable", e);
			throw e;

		}

		return false;
	}

	/* ------------------- QUERY BUILDERS ------------------------- */

	/**
	 * Generates a query to create a table in the database with the given type as
	 * the table name. An example of a query for AR event would be:
	 * 
	 * CREATE TABLE IF NOT EXISTS ar ( event_type string, event_probability float,
	 * ... PRIMARY KEY (kd_archivid) );
	 * 
	 * @param type solar even type
	 * @return the generated query
	 */
	private String generateQuery_createTable(EventType type, String schemaname) {
		String tableName = type.toQualifiedString();

		StringBuilder builder = new StringBuilder();
		builder.append("CREATE TABLE IF NOT EXISTS ");
		builder.append(schemaname + ".");
		builder.append(tableName);
		builder.append(" ( ");

		// Get all attributes for this event
		Set<String> allAttributes = this.eventAttribMap.get(type);
		// For each attribute, find its corresponding type
		for (String attrib : allAttributes) {
			String colType = this.attributesTypeMap.get(attrib);
			if (attrib.contains("sdb_id"))
				continue;
			builder.append(attrib + " " + colType + ", ");
		}
		// Removes the extra comma at the end
		builder.deleteCharAt(builder.length() - 1);

		// Finally, add the extra spatial objects as well.
		for (String attrib : this.geometryAttributes) {
			if (attrib.contains("_spatialobj")) {
				builder.append(attrib + " geometry, ");
			} else {
				builder.append(attrib + " text, ");
			}
		}

		builder.deleteCharAt(builder.length() - 1);

		builder.append(" sdb_id SERIAL PRIMARY KEY");
		builder.append(" );");
		builder.append(" CREATE INDEX ");
		builder.append(tableName);
		builder.append("_kb_archive_idx ON ");
		builder.append(schemaname);
		builder.append(".");
		builder.append(tableName);
		builder.append(" (kb_archivid);");

		return builder.toString();
	}

	/**
	 * SELECT EXISTS ( SELECT 1 FROM pg_tables WHERE schemaname = 'schema_name' AND
	 * tablename = 'table_name' );
	 * 
	 * @param type
	 * @param schema
	 * @return
	 */
	private String generateQuery_eventTableExists(EventType type, String schema) {
		String tableName = type.toQualifiedString();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT EXISTS ( ");
		sb.append("SELECT 1 FROM pg_tables where schemaname ='");
		sb.append(schema);
		sb.append("' AND tablename = '");
		sb.append(tableName);
		sb.append("' );");

		return sb.toString();
	}

}
