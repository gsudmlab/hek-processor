/**
 * hek-processor, a project designed for part of the ISD data pipeline at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 * Copyright (C) 2019 Georgia State University
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.databases;

import java.sql.Connection;
import edu.gsu.cs.dmlab.datatypes.EventType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.*;

import javax.sql.DataSource;

import edu.gsu.cs.dmlab.databases.interfaces.IISDObjectDBConnector;

import edu.gsu.cs.dmlab.datatypes.interfaces.IISDEventReport;
import edu.gsu.cs.dmlab.exceptions.InvalidAttributeException;

/**
 * 
 * @author Azim Ahmadzadeh and Surabhi Priya, Data Mining Lab, Georgia State
 *         University
 *
 */
public class HEKProcessorObjectDBConnector implements IISDObjectDBConnector {

	private final Logger logger;

	private DataSource dsourc;
	/*
	 * Map of the attribute names for each event type
	 */
	private Map<EventType, Set<String>> eventAttribMap;

	/*
	 * Map of <Attribute Name, Attribute type> for the attributes listed in the
	 * above map.
	 */
	private Map<String, String> attributesTypeMap;

	/*
	 * Map of the Names of all the geometry attributes to be used on all the event
	 * types.
	 */
	private Set<String> geometryAttributes;

	/**
	 * Constructor of the object database connection object.
	 * 
	 * @param dsourc             The datasource connection object used to connect to
	 *                           the database.
	 * 
	 * @param map                Map of the attribute names for each event type
	 * 
	 * @param attributesTypeMap  Map of <Attribute Name, Attribute type> for the
	 *                           attributes listed in the above map.
	 * 
	 * @param geometryAttributes Names of all the geometry attributes to be used on
	 *                           all the event types.
	 * 
	 * @param logger             The SLF4J logging object used to log errors that
	 *                           occur in this object.
	 */
	public HEKProcessorObjectDBConnector(DataSource dsourc, Map<EventType, Set<String>> map,
			Map<String, String> attributesTypeMap, Set<String> geometryAttributes, Logger logger) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in HEKProcessorDBConnector constructor.");
		if (map == null)
			throw new IllegalArgumentException("Map cannot be null in HEKProcessorDBConnector constructor.");
		if (attributesTypeMap == null)
			throw new IllegalArgumentException(
					"attributesTypeMap cannot be null in HEKProcessorDBConnector constructor.");
		if (geometryAttributes == null)
			throw new IllegalArgumentException(
					"geometryAttributes cannot be null in HEKProcessorDBConnector constructor.");
		if (logger == null)
			throw new IllegalArgumentException("logger cannot be null in HEKProcessorDBConnector constructor.");

		this.dsourc = dsourc;
		this.eventAttribMap = map;
		this.attributesTypeMap = attributesTypeMap;
		this.geometryAttributes = geometryAttributes;
		this.logger = logger;
	}

	@Override
	public boolean checkReportInDB(EventType type, String kb_archivid) throws SQLException {

		boolean exists = false;

		try (Connection con = this.dsourc.getConnection()) {
			con.setAutoCommit(true);
			String schema = con.getSchema();

			String query = this.generateQuery_checkReportInDB(type, kb_archivid, schema);

			try (PreparedStatement tableExistsPrepStmt = con.prepareStatement(query)) {
				try (ResultSet res = tableExistsPrepStmt.executeQuery()) {
					if (res.next()) {
						exists = res.getBoolean(1);
					}
				}
			}
		} catch (SQLException e) {
			logger.error("SQL Exception while executing method checkReportInDB", e);
			throw e;
		}
		return exists;
	}

	@Override
	public boolean insertEventReport(IISDEventReport eventReport) throws SQLException, InvalidAttributeException {

		// Start insertion
		try (Connection con = this.dsourc.getConnection()) {
			con.setAutoCommit(true);

			// create and execute the query
			String query = this.generateQuery_insertEventReport(eventReport);

			try (PreparedStatement createTableStatement = con.prepareStatement(query)) {
				int affectedRows = createTableStatement.executeUpdate();
				if (affectedRows != 0) {
					return true;
				}
			}
		} catch (SQLException e) {
			logger.error("SQL Exception while executing method insertEventReport", e);
			throw e;
		}
		return false;
	}

	/* ------------------- QUERY BUILDERS ------------------------- */

	/**
	 * Generates the following query: select exists(select 1 from public.ar where
	 * sdb_id=sdb_id)
	 * 
	 * @param type
	 * @param sdb_id
	 * @return
	 */
	private String generateQuery_checkReportInDB(EventType type, String kb_archivid, String schema) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT EXISTS(SELECT 1 FROM ");
		sb.append(schema);
		sb.append(".");
		sb.append(type.toQualifiedString());
		sb.append(" WHERE kb_archivid='");
		sb.append(kb_archivid);
		sb.append("');");

		return sb.toString();

	}

	/**
	 * Generates a query to insert a new record to an event table. This is done in 2
	 * steps: 1. The values for all attributes, both those listed in
	 * 'eventAttribMap' and those listed in 'GEOM_ATTRIBUTES.geometryAttributes',
	 * will be retrieved from the report, and their format will be converted to
	 * postgres formats, 2. The combined attributes-and-values will be inserted to
	 * the table as a new report.
	 * 
	 * @param eventReport an object generated based on the HEK report as a json
	 *                    file.
	 * @return the generated query.
	 * @throws InvalidAttributeException
	 */
	private String generateQuery_insertEventReport(IISDEventReport eventReport) {

		StringBuilder builder = new StringBuilder();
		Map<String, String> postgres_AttributeValues = new HashMap<String, String>();
		EventType eventType = eventReport.getEventType();

		// Get all possible attributes (whether opt or req) corresponding to this event
		Set<String> attributes = this.eventAttribMap.get(eventType);
		Set<String> allAttributes = new HashSet<>(attributes);
		allAttributes.remove("sdb_id"); // since this one (PK) is set to 'auto-increment'

		// Convert values to postgres values
		for (String attr : allAttributes) {
			String postgresValue = getActualPostgreMapping(eventReport, attr);
			postgres_AttributeValues.put(attr, postgresValue);
		}

		// Get all values for the spatial attributes
		for (String attr : this.geometryAttributes) {
			String postgresValue = getActualPostgreMapping(eventReport, attr);
			postgres_AttributeValues.put(attr, postgresValue);
		}

		// Start generating the query:
		builder.append("INSERT INTO " + eventType.toQualifiedString() + " ( ");
		// Add all column names (attributes' name)
		for (Map.Entry<String, String> entry : postgres_AttributeValues.entrySet()) {
			builder.append(entry.getKey() + ",");
		}
		// drop the last comma
		builder.deleteCharAt(builder.length() - 1);
		builder.append(" ) VALUES (");

		// Add all values for each attribute
		for (Map.Entry<String, String> entry : postgres_AttributeValues.entrySet()) {
			builder.append(entry.getValue() + ",");
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.append(" );");

		return builder.toString();
	}

	/* ------------------- HELPER ------------------------- */

	/**
	 * Converts the existing values of the given attribute to the postgre format.
	 * 
	 * @param eventReport
	 * @param attribute
	 * @return
	 * @throws InvalidAttributeException
	 */
	private String getActualPostgreMapping(IISDEventReport eventReport, String attribute) {

		try {
			String attributeValue = "";

			if (this.geometryAttributes.contains(attribute.toLowerCase())) {

				// For Spatial objects to be stored as 'polygon'
				if (attribute.contains("_spatialobj")) {

					attributeValue = eventReport.getAttr(attribute.replace("_spatialobj", ""));
					if (attributeValue == null || attributeValue.isEmpty()) {
						return null;
					}
					return "ST_GeomFromText('" + attributeValue + "',4326)";

				}
				// For spatial objects to be stored as 'text'
				else {

					attributeValue = eventReport.getAttr(attribute);
					if (attributeValue == null || attributeValue.isEmpty()) {
						return null;
					}
					String val = "'" + (attributeValue != null ? attributeValue.replaceAll("'", " ") : "") + "'";
					return val;

				}

			}

			if (attribute.equalsIgnoreCase("event_starttime")) {
				attributeValue = eventReport.getAttr(attribute);
				String val = "'" + (attributeValue != null ? attributeValue.replaceAll("'", " ") : "") + "'";
				return val;
			} else if (attribute.equalsIgnoreCase("event_endtime")) {
				String attributeValue_startTime = "";
				String attributeValue_endTime = "";
				attributeValue = eventReport.getAttr(attribute);
				attributeValue_startTime = eventReport.getAttr("event_starttime");
				attributeValue_endTime = eventReport.getAttr("event_endtime");

				if (attributeValue_endTime.equalsIgnoreCase(attributeValue_startTime)) {
					String val = "TIMESTAMP '" + (attributeValue != null ? attributeValue.replaceAll("'", " ") : "")
							+ "' + INTERVAL '1 second'";

					return val;
				}
				String val = "'" + (attributeValue != null ? attributeValue.replaceAll("'", " ") : "") + "'";
				return val;
			}

			if (!this.attributesTypeMap.containsKey(attribute)
					|| this.attributesTypeMap.get(attribute).equalsIgnoreCase("text")) {
				attributeValue = eventReport.getAttr(attribute);
				String val = "'" + (attributeValue != null ? attributeValue.replaceAll("'", " ") : "") + "'";
				return val;
			}

			if (attributeValue == null || attributeValue.length() == 0)
				return "0";
			return attributeValue;

		} catch (Exception e) {
			return "0";
		}
	}

}
