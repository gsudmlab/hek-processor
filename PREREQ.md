* [Return to READEME](./README.md)

# PREREQUISITES 
__Note:__ These instructions have only been tested on Ubuntu 18.04, and are only meant as a helpful guide, not a definitive set of instructions.

### Step 1: Update your machine ###

	sudo apt-get update -y
	sudo apt-get upgrade -y

### Step 2: Install Java JRE and JDK ###
Apache Maven requires Java to be installed on your machine. So, install the latest default version of OpenJDK jre: 

	sudo apt install default-jre

Verify the Java version by running the following command:

	java -version

The reply should be something like this.

	openjdk version "11.0.4" 2019-07-16
	OpenJDK Runtime Environment (build 11.0.4+11-post-Ubuntu-1ubuntu218.04.3)
	OpenJDK 64-Bit Server VM (build 11.0.4+11-post-Ubuntu-1ubuntu218.04.3, mixed mode, sharing)

You will need the Java Development Kit (JDK) in addition to the JRE in order to compile and run some specific Java-based software. To install the JDK, execute the following command, which will also install the JRE:

	sudo apt install default-jdk

Verify that the JDK is installed by checking the version of javac, the Java compiler:

	javac -version

You'll see output similar to the following:

	javac 11.0.4

### Step 3: Install Apache Maven ###
To install maven execute the following:

	sudo apt install maven

Then test it with the command

	mvn -version
	
The reply should be something like this.

	Apache Maven 3.3.9
	Maven home: /usr/share/maven
	Java version: 1.8.0_91, vendor: Oracle Corporation
	Java home: /usr/lib/jvm/java-8-oracle/jre
	Default locale: en_GB, platform encoding: UTF-8
	OS name: "linux", version: "4.4.0-31-generic", arch: "amd64", family: "unix"
		
### Step 4: Install Git ###

This project is stored in a git repository, and if you don't have git installed through an IDE, you will need to install it to make copying it to your computer easier. 

To install simply do:
	
	sudo apt-get install git
	
### Step 5: Maven Setup ###
One thing that seems to have been missed by the maven and java package installations is providing the JAVA_HOME that is expected by the maven compiler plugin.  To accomplish that just for the maven program, we can specify it in a config file in the home directory.  This file is named .mavenrc and should include the line:

	export JAVA_HOME=/usr/lib/jvm/default-java

### Step 6: Install DMLabLib ###
This project uses parts of the Open Source library developed by [GSU Data Mining Lab](http://dmlab.cs.gsu.edu).  We utilize the v0.0.3 fork for this project, which is still actively developed and changing so the usages in our pom.xml file references the SNAPSHOT as opposed to a stable version. 
 
Go Here for installation instructions for the library.

[DMLabLib](https://bitbucket.org/gsudmlab/dmlablib/)
	
Usage:

	<dependency>
		<groupId>edu.gsu.cs.dmlab</groupId>
		<artifactId>lib</artifactId>
		<version>0.0.4-SNAPSHOT</version>
	</dependency>

That's it, return to [README](./README.md)